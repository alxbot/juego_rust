/*
Hola Soy Alex soy un diseñador gráfico y he creado como parte de mis ejercicios con RUST 
un pequeñísimo juego.
Es un juego de consola de comandos simple, por turnos y mediado por el azar,
casi como un cara y sello solo que un poquitín más divertido.
También lo he colgado como guía para quien esté empezando con RUST <3 y pueda entender
algunas partes de los conceptos básicos de variables, funciones, estructuras, implementaciones y referencias
Saludos
*/


//Importo el módulo juego que trae la lógica del combate
mod juego;
//Hago uso de la importación y determino que usaré específicamente la estructura Guerrero (Nota: me hace recordar los namespaces de c++)
use crate::juego::guerreros::{Guerrero};

fn main() {
   
//Personaje 1  
let mut pj1 = Guerrero{
        raza:"Orco".to_string(),
        vida:100,
        fuerza:25
};

//Personaje 2
let mut pj2 = Guerrero{
        raza:"Humano".to_string(),
        vida:100,
        fuerza:20
};


//Combate planeado a 9 turnos

for a in 0..9 {
    if pj1.vida <= 1 || pj2.vida <= 1 //Si alguno de los 2 personajes tiene vida menor a 1 
    {
        //Si el personaje 1 tiene vida menor a 1
        if pj1.vida <1{
            //El personaje 1 ha muerto
            println!("El {:?} ha muerto ", pj1.raza);
            println!("El Ganador es {:?} ", pj2.raza);

        } else if pj2.vida<1{ //Pero si el personaje 2 tiene -1 de vida
            //Devuelve el personaje2 está muerto
            println!("El {:?} ha muerto ", pj2.raza);
            println!("El Ganador es {:?} ", pj1.raza);
        }
        //Devuelve la cantidad de rondas que se necesitó para terminar el combate
        println!("El combate duró {} rondas", a);

        //Elimina la iteración porque ya hay un personaje muerto
        break;

    }else{ //Si ningún personaje tiene vida -1 entonces que sigan peleando
        //Se ejecuta otra ronda
        juego::combate(&mut pj1,&mut pj2);
        
    }

}

//Fin de la iteración del combate
 
}


