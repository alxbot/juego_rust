//Importo y Uso el crate rand
use rand;
//Importo el móudulo guerreros que está dentro de la carpeta juego
pub mod guerreros;
//Uso el modulo guerreros y especifico que importaré directamente la estructura Guerrero
use crate::juego::guerreros::{Guerrero};

//Creo la función del combate que recibe 2 referencias mutables a cada guerrero
pub fn combate(p1:&mut Guerrero,p2:&mut Guerrero){
    //Creo variables para almacenar quién sera el atacante y receptor
    let atacante;
    let receptor;  
    //Random booleano  me permite definir si true o false  
    let elegido:bool = rand::random();
       
    //Si se define true entonces el atacante será el personaje 1   
        if elegido{
            atacante = p1; //Se le asigna a la variable atacante el personaje 1 (Recordar: p1 es una referencia mutable)
            receptor = p2; //Se le asigna a la variablereceptor el personaje 2 (Recordar: p2 es una referencia mutable) 
        }else{ //si se define false entonces el atacante será el personaje 2
            atacante =p2; //Se le asigna a la variable atacante el personaje 2 (Recordar: p2 es una referencia mutable)
            receptor =p1; //Se le asigna a la variable atacante el personaje 1 (Recordar: p1 es una referencia mutable)
        }

        //Quien es ahora el atacante se ejecutará el método atacar donde pasa como parámetro al receptor (referencia mutable)
        atacante.atacar(receptor);
             
}