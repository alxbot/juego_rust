#[derive(Debug)] //Se coloca el atributo para permitir el debugging y la visualización de estructuras
//Se crea la estructura pública llamada Guerrero con 3 campos raza vida y fuerza
pub struct Guerrero{
        pub raza:String, //El campo recibe un dato tipo String (heap)
        pub vida:i32, //El campo recibe un dato tipo isigned de 32bits
        pub fuerza:i32, //El campo recibe también un dato tipo isigned 32bits
    }


//Se crea la implementación para gestionar un método que será atacar
impl Guerrero{
    //Se crea el método atacar que recibe como parámetro self ( una referencia a sí mismo ) y receptor que es una referencia mutable de una estructura tipo guerrero
    pub fn atacar(&self,receptor:&mut Guerrero){
        //atacar -> El método consiste en reducir la vida del receptor con la fuerza del atacante          
        receptor.vida-=&self.fuerza;
        //Imprime el estado del combate con la información de quién ataca, con la fuerza del ataque, quién recibe el ataque y la actualización de la vida del receptor
        println!("El {:?} ha atacado con {:?} de fuerza al {:?}, ahora el {:?} tiene {:?} de vida",&self.raza,&self.fuerza,receptor.raza,receptor.raza,receptor.vida);    
    }
}//Fin de la implementación


